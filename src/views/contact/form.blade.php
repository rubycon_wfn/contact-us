@extends('layouts.app')

@section('meta_title', (\Settings::getConfigValue('contacts/page_title') ?: __('Contact Us')) . ' | ' . \Settings::getConfigValue('app/name'))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if(session('success'))
                    <div class="alert alert-success" role="alert">
                        {{ __('Thanks you for contacting us. We will contact you as soon as possible') }}
                    </div>
                @elseif(session('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('error') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">{{ \Settings::getConfigValue('contacts/page_title') ?: __('Contact Us') }}</div>
                    <div class="card-body">
                        <form method="post">
                            @csrf
                            <div class="form-group">
                                <label for="first_name">{{ __('First Name') }} <em>*</em></label>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="{{ __('First Name') }}">
                            </div>
                            <div class="form-group">
                                <label for="last_name">{{ __('Last Name') }} <em>*</em></label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="{{ __('Last Name') }}">
                            </div>
                            <div class="form-group">
                                <label for="email">{{ __('Email') }} <em>*</em></label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="{{ __('Email') }}">
                            </div>
                            @if($subjects = \WFN\ContactUs\Helper\Data::getSubjectOptions())
                            <div class="form-group">
                                <label for="subject">{{ __('Subject') }} <em>*</em></label>
                                <select id="subject" name="subject" class="form-control">
                                @foreach($subjects as $subject)
                                    <option value="{{ $subject['id'] }}">{{ $subject['text'] }}</option>
                                @endforeach
                                </select>
                            </div>
                            @endif
                            <div class="form-group">
                                <label for="message">{{ __('Message') }} <em>*</em></label>
                                <textarea class="form-control" id="message" name="message"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection