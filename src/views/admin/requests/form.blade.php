@extends('admin::layouts.app')

@section('content')
<div class="container-fluid form">
    <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <div class="btn-group float-sm-right">
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="setLocation('{{ route('admin.contact.requests') }}')">
                    <i class="fa fa-arrow-circle-o-left"></i>
                    <span>Back</span>
                </button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header text-uppercase">{{ __('Contact Form Request') }}</div>
                <div class="card-body">
                    <form>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ __('First Name') }}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" value="{{ $request->first_name }}" disabled="disabled" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ __('Last Name') }}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" value="{{ $request->last_name }}" disabled="disabled" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ __('Email') }}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" value="{{ $request->email }}" disabled="disabled" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ __('Subject') }}</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" value="{{ $request->subject_label }}" disabled="disabled" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ __('Message') }}</label>
                            <div class="col-sm-9">
                                <textarea rows="20" class="form-control" disabled="disabled">{{ $request->message }}</textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection