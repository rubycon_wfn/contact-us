@extends('admin::layouts.app')

@section('content')
<div class="container-fluid grid">
    <div class="row pt-2 pb-2">
        <div class="col-sm-12"><h4 class="page-title">{{ __('Contact Us Requests') }}</h4></div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Total {{ $requests->total() }} records found</h5>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="align-top"><strong>{{ __('First Name') }}</strong></th>
                                    <th class="align-top"><strong>{{ __('Last Name') }}</strong></th>
                                    <th class="align-top"><strong>{{ __('Email') }}</strong></th>
                                    <th class="align-top"><strong>{{ __('Subject') }}</strong></th>
                                    <th class="align-top"><strong>{{ __('Date') }}</strong></th>
                                    <th class="align-top"><strong>{{ __('Status') }}</strong></th>
                                    <th class="align-top" width="10%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($requests))
                                @foreach($requests as $request)
                                <tr>
                                    <td style="white-space: normal;">{{ substr($request->first_name, 0, 30) }}</td>
                                    <td style="white-space: normal;">{{ substr($request->last_name, 0, 30) }}</td>
                                    <td style="white-space: normal;">{{ $request->email }}</td>
                                    <td style="white-space: normal;">{{ $request->subject_label }}</td>
                                    <td>{{ $request->created_at->format('M j, Y H:i') }}</td>
                                    <td style="white-space: normal;">
                                        @if(!$request->status)
                                            <div class="progress"><div class="progress-bar bg-success" style="width:100%">{{ __('New') }}</div></div>
                                        @else
                                            <div class="progress"><div class="progress-bar bg-warning" style="width:100%">{{ __('Viewed') }}</div></div>
                                        @endif
                                    </td>
                                    <td>
                                        @if(Auth::guard('admin')->user()->role->isAvailable('admin.contact.requests.view'))
                                        <button type="button" class="btn btn-primary waves-effect waves-light ml-2 btn-sm" 
                                            onclick="setLocation('{{ route('admin.contact.requests.view', ['id' => $request->id]) }}')">
                                            <i class="fa icon-eye"></i>
                                            <span></span>
                                        </button>
                                        @endif
                                        @if(Auth::guard('admin')->user()->role->isAvailable('admin.contact.requests.delete'))
                                        <button type="button" class="btn btn-danger waves-effect waves-light ml-2 btn-sm" 
                                            onclick="confirmSetLocation('{{ route('admin.contact.requests.delete', ['id' => $request->id]) }}')">
                                            <i class="fa fa-trash-o"></i>
                                            <span></span>
                                        </button>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="2">{{ __('There are no items found') }}</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{ $requests->links('admin::widget.pagination') }}

        </div>
    </div>
</div>
@includeIf('admin::widget.confirmation')

@endsection