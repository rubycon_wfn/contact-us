<?php
namespace WFN\ContactUs\Http\Controllers\Admin;

use WFN\ContactUs\Model\Request as ContactRequest;

class IndexController extends \WFN\Admin\Http\Controllers\Controller
{

    public function index()
    {
        $requests = ContactRequest::orderBy('created_at', 'desc')->paginate(20);
        return view('contact::admin.requests.grid', compact('requests'));
    }

    public function view($id)
    {
        try {
            $request = ContactRequest::findOrFail($id);
            $request->status = 1;
            $request->save();
            return view('contact::admin.requests.form', compact('request'));
        } catch (\Exception $e) {
            \Alert::addError($e->getMessage());
            return redirect()->back();
        }
    }

    public function delete($id)
    {
        try {
            ContactRequest::findOrFail($id)->delete();
            \Alert::addSuccess(__('Contact Us Request has been removed successfully!'));
        } catch (\Exception $e) {
            \Alert::addError(__('Something went wrong. Please, try again'));
        }
        return redirect()->back();
    }

}
