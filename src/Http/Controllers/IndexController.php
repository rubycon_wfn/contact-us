<?php

namespace WFN\ContactUs\Http\Controllers;

use WFN\ContactUs\Model\Request as ContactRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Settings;

class IndexController extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        return view('contact.form');
    }

    public function post(Request $request)
    {
        try {
            
            $success = false;
            $data = $request->all();
            $this->validator($data)->validate();
            ContactRequest::create($data);
            $success = true;
            $error = false;

            $this->_nofifyByEmail($data);

        } catch (ValidationException $e) {
            $error = [];
            foreach($e->errors() as $messages) {
                $error = array_merge($error, $messages);
            }
            $error = implode(' ', $error);
        } catch (\Exception $e) {
            $error = __('Something went wrong. Please try again later');
        }

        return redirect()->route('contacts')->with('success', $success)->with('error', $error);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name'  => 'required|string|max:255',
            'email'      => 'required|string|max:255',
            'message'    => 'required|string',
        ]);
    }

    protected function _nofifyByEmail($data)
    {
        try {
            $emailTemplate = \Settings::getConfigValue('contacts/email_template');

            $subjects = json_decode(\Settings::getConfigValue('contacts/subjects'), true);
            if($subjects && empty($data['subject'])) {
                foreach($subjects as $subject) {
                    if($subject['identifier'] == $data['subject']) {
                        $emailRecipient = $subject['recipient'];
                        break;
                    }
                }
            } else {
                $emailRecipient = \Settings::getConfigValue('contacts/recipient_email');
            }

            if (!$emailTemplate || empty($emailRecipient)) {
                throw new \Exception();
            }
            \MandrillMail::send($emailTemplate, $emailRecipient, $data);
        } catch (\Exception $e) {
            // pass
        }
    }

}