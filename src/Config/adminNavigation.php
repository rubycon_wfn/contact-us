<?php

return [
    'contact-us' => [
        'route' => 'admin.contact.requests',
        'icon'  => 'icon-paper-plane',
        'label' => 'Contact Us Requests',
    ],
];