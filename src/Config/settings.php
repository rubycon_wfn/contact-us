<?php

return [
    'contacts' => [
        'label'  => 'Contact Us',
        'fields' => [
            'url_path' => [
                'label' => 'URL Path',
                'type'  => 'text',
            ],
            'page_title' => [
                'label' => 'Page Title',
                'type'  => 'text',
            ],
            'subjects' => [
                'label'   => 'Subjects',
                'type'    => 'rows',
                'columns' => [
                    'title' => [
                        'label' => 'Title',
                        'type' => 'text'
                    ],
                    'identifier' => [
                        'label' => 'Identifier',
                        'type' => 'text'
                    ],
                    'recipient' => [
                        'label' => 'Recipient Email',
                        'type' => 'text'
                    ],
                ],
            ],
            'recipient_email' => [
                'label' => 'Recipient Email',
                'type'  => 'text',
            ],
            'email_template' => [
                'label' => 'Email Template',
                'type'  => 'text',
            ],
        ],
    ],
];