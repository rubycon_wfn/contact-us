<?php
namespace WFN\ContactUs\Providers;

use WFN\Admin\Providers\ServiceProvider as WFNServiceProvider;
use Illuminate\Support\Facades\Route;

class ServiceProvider extends WFNServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $basePath = realpath(__DIR__ . '/..');
        Route::middleware('web')->group($basePath . '/routes/web.php');
        
        $this->loadViewsFrom($basePath . '/views', 'contact');
        $this->publishes([
            $basePath . '/views' => resource_path('views'),
        ], 'view');

        $this->loadMigrationsFrom($basePath . '/database/migrations');
        $this->mergeConfigFrom($basePath . '/Config/adminNavigation.php', 'adminNavigation');
        $this->mergeConfigFrom($basePath . '/Config/settings.php', 'settings');

        $router->aliasMiddleware('cms', \WFN\CMS\Http\Middleware\Router::class);
    }

}
