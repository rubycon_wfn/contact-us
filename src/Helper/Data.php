<?php

namespace WFN\ContactUs\Helper;

class Data
{

    public static function getSubjectOptions()
    {
        $subjects = json_decode(\Settings::getConfigValue('contacts/subjects'), true);
        $preparedSubjects = [];
        if(is_array($subjects)) {
            foreach($subjects as $subject) {
                if(empty($subject['identifier']) || empty($subject['title'])) {
                    continue;
                }
                $preparedSubjects[] = [
                    'id'   => $subject['identifier'],
                    'text' => $subject['title']
                ];
            }
        }
        return $preparedSubjects;
    }
}