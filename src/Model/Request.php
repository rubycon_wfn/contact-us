<?php

namespace WFN\ContactUs\Model;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{

    protected $table = 'contact_request';

    protected $fillable = ['first_name', 'last_name', 'email', 'subject', 'message', 'status'];

    public function getSubjectLabelAttribute($value = false)
    {
        $subjects = json_decode(\Settings::getConfigValue('contacts/subjects'), true) ?: [];
        foreach($subjects as $subject) {
            if(optional($subject)['identifier'] == $this->subject) {
                return optional($subject)['title'];
            }
        }
        return 'N/A';
    }

}