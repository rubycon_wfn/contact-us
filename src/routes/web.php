<?php

$contactsRoute = \Settings::getConfigValue('contacts/url_path') ?: 'contact';
Route::prefix($contactsRoute)->group(function() {
    Route::get('/', 'WFN\ContactUs\Http\Controllers\IndexController@index')->name('contacts');
    Route::post('/', 'WFN\ContactUs\Http\Controllers\IndexController@post');
});

Route::prefix(env('ADMIN_PATH', 'admin'))->group(function() {
    Route::prefix('contacts')->group(function() {
        Route::get('/', '\WFN\ContactUs\Http\Controllers\Admin\IndexController@index')->name('admin.contact.requests');
        Route::get('/view/{id}', '\WFN\ContactUs\Http\Controllers\Admin\IndexController@view')->name('admin.contact.requests.view');
        Route::get('/delete/{id}', '\WFN\ContactUs\Http\Controllers\Admin\IndexController@delete')->name('admin.contact.requests.delete');
    });
});